require 'active_support/core_ext/string' # String.blank? shortcut
require 'highest/version'

##
# This module takes a well formated data file and returns the IDs of the top scoring object,
# along with the scores themselves as well formated JSON.
#
# Author:: David W. Keith <git@dwk.io>
# Licence:: MIT
module Highest

  ##
  # A input formatting error indicates that the input did
  # not conform to the agreed upon specification.
  class InputFormattingError < StandardError
  end

  ##
  # A file not found error indicates that the path did not point to
  # a valid file.
  class FileNotFoundError < StandardError
  end

  ##
  # A simple class for manipulating a data file.
  class DataFile

    ##
    # Sets up the datafile for processing
    def initialize(file_path)
      @records = []
      read_file(file_path)
      parse_raw_data
    end # intialize()

    ##
    # Returns the top scoring records up to the passed in count
    def top(count=5)
      @records.sort{ |a, b| b[:score] <=> a[:score] }[0, count]
    end # top()

    private

    ##
    # parses the raw data from the file, sepreate method for ease of debugging.
    #
    # may raise an `InputFormattingError` if the file is not formatted to spec.
    def parse_raw_data()
      @raw_data.each_line do |line|
        # Empty lines should be ignored rather than treated as improperly formatted.
        unless line.empty? or line.blank?
          begin
            record = line.split(':', 2)
            id = parse_json(record[1])['id']
            if id.nil?
              raise InputFormattingError.new('Input formatting error, missing id')
            end
            @records.push({
              :score => record[0],
              :id => id
            })
          rescue Exception => ex
            raise InputFormattingError.new('Input formatting error, unable to parse line.')
          end
        end # unless
      end
    end # parse_raw_data()

    ##
    # Reads the specified `file_path` into memory.
    #
    # may raise a `FileNotFoundError` if the file is not found.
    # may rais a 'RuntimeError` if `File.open` fails in an unhandled way.
    def read_file(file_path)
      begin
        @raw_data = File.open(file_path).read
      rescue Exception => ex
        if ex.class == Errno::ENOENT
          raise FileNotFoundError.new('File not found.')
        else
          raise RuntimeError.new('Unknown error reading file.')
        end
      end
    end # read_file()

    ##
    # Pasrses the specified `str` as JSON.
    #
    # may raise a `InputFormattingError` if the string is not valid JSON.
    # may rais a 'RuntimeError` if `JSON.parse` fails in an unhandled way.
    def parse_json(str)
      begin
        json = JSON.parse(str)
      rescue Exception => ex
        puts ex.message
        if ex.class == JSON::ParserError
          raise InputFormattingError.new('Input formatting error, unable to parse JSON.')
        else
          raise RuntimeError.new('Unknown error parsing JSON.')
        end
      end
      json
    end # parse_json()
  end # class DataFile
end
