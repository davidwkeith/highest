#!/usr/bin/env ruby
require "bundler/setup"
require "highest"
require "json"
require "optparse"

Options = Struct.new(:file, :count)

##
# Not specified in the instructions, but really we should be using flags for options.
# Flags, especially `--` ones, allow for easier debugging of scripts that use this tool.
# Here we set up a standard `OptionParser` to parse the flagged options. Nothing is required
# to allow us to fall back to the specified options.
class HighestCLI

  ##
  # Standard parser
  def self.parse(options)
    args = Options.new()

    opt_parser = OptionParser.new do |opts|
      opts.banner = 'Usage: highest [options]'

      opts.on('-f FILE', '--file=FILE', 'Data file to parse') do |f|
        args.file = f
      end

      opts.on('-c COUNT', '--count=COUNT', Integer, 'Count of records to return') do |c|
        args.count = c
      end

      opts.on('-h', '--help', 'Prints help') do
        puts 'A simple script to parse scored data files where each line is in the format `<score>:<json_dictionary>`.'
        puts
        puts opts
        exit
      end
    end

    opt_parser.parse!(options)
    return args
  end
end

##
# Do It All: this is where the CLI magic happens
# We will exit with one of the following conditions:
# * 0: no errors
# * 1: data file not found
# * 2: data file not formatted correctly
# * 3: unknown error
#
# all errors will be written to `STDERR` for review.
begin
  options = HighestCLI.parse(ARGV)

  # The requirements as for unnamed parameters, which isn't best practice
  # This allows for non-standard unnamed parameters to be optionally passed.
  if options.file.nil?
    options.file = ARGV[0]
    options.count = ARGV[1].to_i
  end
  if options.file.nil? or options.count == 0
    HighestCLI.parse(['-h'])
    exit 3
  end

  data = Highest::DataFile.new(options.file)
  # we could also save to a file here
  puts JSON.pretty_generate(data.top(options.count))
rescue Exception => ex
  # Input formatting errors should exit with code 2 while file not found should exit with code 1
  if ex.class == Highest::FileNotFoundError
    STDERR.puts "File not found: <#{options.file}>"
    exit 1
  elsif ex.class == Highest::InputFormattingError
    STDERR.puts "Formattting error in <#{options.file}>"
    exit 2
  else # All other errors exit 3
    STDERR.puts ex
    exit 3
  end
end
