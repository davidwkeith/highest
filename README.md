# Highest

Highest is a simple gem to parse a data file and output the top scoring records from that file with the record IDs.

The data file is expected to be in the following format:

```json
8795136:{"id":"d2e257c282b54347ac14b2d8d0da814c","x":"foo","y":7,"z":11, "payload":"somelargeamountofdata"}
5317020:{"id":"619236365add4a0ca6e501fc62cfbaba","type":"purple","payload":"some smallamountofdata"}
...
```

The instructions used the phrase "some large amount of data", but the assumptions I made was that the entire file would fit in memory. If this is not the case, this should be modified to only read the file line by line rather than all at once. This assumption was mainly made for readability reasons.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'highest'
```

And then execute:

```
bundle
```

Or install it yourself as:

```
gem install highest
```

## Usage

Once installed simple run `highest -f [data_file] -c [count]`

The data file is described above, COUNT refers to the number of top scoring record IDs & scores to return as simple JSON.

Possible exit codes are:
* 0 no issues
* 1 file not found
* 2 data file parse error
* 3 unknown error

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing
